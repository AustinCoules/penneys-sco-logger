/*
Program Description: Logs random checks on Penneys SCOs

Author: Austin Coules

Date: 17/11/2023
*/

#include <stdio.h>
#include <time.h>

#define LOGCOUNT 10

void logPrint(int i, int scoNum[LOGCOUNT], char randCheck[LOGCOUNT], char timeArray[LOGCOUNT][20])
{
    for(i = 0; i < LOGCOUNT; i++)
    {
        printf("%d\t%d\t%c\t\t%s\n", (i + 1), scoNum[i], randCheck[i], timeArray[i]);
    }
}

int main()
{
    int i;
    int scoNum[LOGCOUNT];
    char randCheck[LOGCOUNT];
    time_t rawtime;
    struct tm * timeinfo;
    char timeArray[LOGCOUNT][20];

    for(i = 0; i < LOGCOUNT; i++)
    {
        printf("Please enter the SCO number:\n");
        scanf("%d", &scoNum[i]);

        printf("Random check? Y/N Default = N:\n");
        while(getchar() != '\n');
        scanf("%c", &randCheck[i]);

        switch (randCheck[i])
        {
        case 'Y':
            break;
        
        case 'y':
        randCheck[i] = 'Y';
            break;

        case 'N':
            break;
        
        case 'n':
        randCheck[i] = 'N';
        
        default:
        randCheck[i] = 'N';
            break;
        }

    time (&rawtime);
    timeinfo = localtime (&rawtime);

    strftime(timeArray[i], sizeof(timeArray[i]), "%d/%m/%y %H:%M:%S", timeinfo); // Format time as dd/mm/yy hh:mm:ss
    }

    printf("\nNo:\tSCO No:\tRandom Check?\tTime:\n");
    logPrint(i, scoNum, randCheck, timeArray);

    return 0;
}