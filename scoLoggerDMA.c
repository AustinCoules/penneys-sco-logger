/*
Program Description: Logs transaction activity for Penneys Self Checkouts, defining the log length using dynamic memory allocation.

Author: Austin Coules

Date: 29/11/2023

TODO:
- Add time as a log entry
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{

  int i; // Incrementor for log events
  int logCount; // The number of log events
  int allocSize; // How much memory to allocate to both scoNum and randCheck
  int *scoNum; // Stores each entry of SCO number for the log event
  char *randCheck; // Stores each entry of whether a random check occurred during the log event

  printf("How many logs do you want to create?\n");
  scanf("%d", &logCount);

// Allocate memory for scoNum
  allocSize = logCount * sizeof(int);
  scoNum = malloc(allocSize);

  if(scoNum == NULL)
  {
    printf("Failed to allocate memory for scoNum.");
  }
  else
  {
// Allocate memory for randCheck
    allocSize = logCount * sizeof(char);
    randCheck = malloc(allocSize);

    if(randCheck == NULL)
    {
      printf("Failed to allocate memory for randCheck.");
    }
    else
    {
// Scan in log info
      for(i = 0; i < logCount; i++)
      {
    // Scan in scoNum
        printf("Please enter the SCO number:\n");
        scanf("%d", &*(scoNum + i));

    // Scan in randCheck
        printf("Random check? Y/N Default = N\n");
        while(getchar() != '\n');
        scanf("%c", &*(randCheck + i));

        switch(*(randCheck + i))
        {
          case 'Y':
            break;
          case 'y':
            *(randCheck + i) = 'Y';
            break;
          default:
            *(randCheck + i) = 'N';
        }
      }
// Print log
      printf("No:\tSCO No:\tRandom Check?\n");
      for(i = 0; i < logCount; i++)
      {
        printf("%d\t%d\t%c\n", (i + 1), *(scoNum + i), *(randCheck + i));
      }
    }
  }

  return 0;
}
